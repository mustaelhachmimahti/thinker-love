var colors = require('colors');
const people = require('./people');
const tr = require('./trinker');

console.log(tr.title());
console.log("Model des données : ");
console.log(people[0]);
console.log(tr.line('LEVEL 1'));
console.log("Nombre d'hommes : ",                                                                   tr.allMale(people).length);
console.log("Nombre de femmes : ",                                                                  tr.allFemale(people).length);
console.log("Nombre de personnes qui cherchent un homme :",                                         tr.nbOfMaleInterest(people).length);
console.log("Nombre de personnes qui cherchent une femme :",                                        tr.nbOfFemaleInterest(people).length);
console.log("Nombre de personnes qui gagnent plus de 2000$ :",                                      tr.moneyFilter(people, 2000).length);
console.log("Nombre de personnes qui aiment les Drama :",                                           tr.dramaLove(people).length);
console.log("Nombre de femmes qui aiment la science-fiction :",                                     tr.fammFicctionaLove(people).length);
console.log(tr.line('LEVEL 2'));
console.log("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ :",          tr.docsAndMoney(people).length);
console.log("Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ :",      tr.listPeopleNLM(people));
console.log("Homme le plus riche (nom et id) :",                                                    tr.richestMan(people));
console.log("Salaire moyen :",                                                                      tr.moyanTotal(people));
console.log("Salaire médian :",                                                                     tr.median(people));
console.log("Nombre de personnes qui habitent dans l'hémisphère nord :",                            tr.northHemispherePeople(people, 0).length);
console.log("Salaire moyen des personnes qui habitent dans l'hémisphère sud :",                     tr.salaireSud(people));
console.log(tr.line('LEVEL 3'));            
console.log("Personne qui habite le plus près de Bérénice Cawt (nom et id) :",                      tr.getDistanceBetweenPeople(people, "Bérénice", "Cawt", 1));
console.log("Personne qui habite le plus près de Ruì Brach (nom et id) :",                          tr.getDistanceBetweenPeople(people, "Ruì", "Brach", 1));
console.log("les 10 personnes qui habite les plus près de Josée Boshard (nom et id) :",             tr.getDistanceBetweenPeople(people, "Josée", "Boshard", 10));
console.log("Les noms et ids des 23 personnes qui travaillent chez google :",                       tr.getGoogleEmployers(people));
console.log("Personne la plus agée :",                                                              tr.getOldest(people));
console.log("Personne la plus jeune :",                                                             tr.getYoungest(people));
console.log("Moyenne des différences d'age :",                                                      tr.averageAge(people));
console.log(tr.line('LEVEL 4'));            
console.log("Genre de film le plus populaire :",                                                    tr.getPopularFilm(people));
console.log("Genres de film par ordre de popularité :",                                             tr.orderPopularGener(people));
console.log("Liste des genres de film et nombre de personnes qui les préfèrent :",                  tr.orderPopularGenerAndPeoplesNumber(people));
console.log("Age moyen des hommes qui aiment les films noirs :",                                    tr.averageAgeFilm(people, "Film-Noir", tr.allMale(people)));
console.log(`Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire 
de Paris et gagnent moins que la moyenne des hommes :`,                                             tr.averageAgeFilmFemParis(people, "Film-Noir",tr.allFemale(people), 48.860073462019386, 2.357357862518512));
console.log(`Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une 
préférence de film en commun (afficher les deux et la distance entre les deux):`,                   tr.ManAndMan(people));
console.log("Liste des couples femmes / hommes qui ont les même préférences de films :",            tr.manAndWoman(people));
console.log(tr.line('MATCH'));
/* 
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui sont les plus proches.
    Puis ceux qui ont le plus de goût en commun.
    Pour les couples hétéroséxuel on s'assure que la femme gagne au moins 10% de moins que l'homme mais plus de la moitié.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi les films d'aventure.
    Le différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agée des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.   ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction   ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum   ߷    ߷    ߷    
*/
console.log("liste de couples à matcher (nom et id pour chaque membre du couple) :",             tr.match(people));