const { find } = require("geo-tz");

//====================================================IMPORTANT===================================================//
//                                                                                                                //
//                         Il est important de lire les commentaires pour comprendre le code.                     //
//                                                                                                                //
//================================================================================================================//
module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  //=============================Start Levl1===========================

  allMale: function ([...p]) {
    const allMen = p.filter(e => e.gender == "Male");
    return allMen;
  },

  allFemale: function ([...p]) {
    const allGirl = p.filter(e => e.gender == "Female");
    return allGirl;
  },

  // First 2
  nbOfMaleInterest: function ([...p]) {
    return p.filter(e => e.looking_for == "M");
  },

  nbOfFemaleInterest: function ([...p]) {
    return p.filter(e => e.looking_for == "F");
  },

  // Interest
  moneyFilter: function ([...p], parm) {
    return p.filter(function (e) {
      let money = parseFloat(e.income.substring(1));
      return money > parm;
    });
  },

  filterFilm: function ([...p], film_param, genre) {
    let array = [];
    const allMen = genre;
    allMen.map(function (e) {
      if (e.pref_movie.includes(film_param)) {
        array.push(e);
      }
    });
    return array;
    //=================================
  },
  //Money
  findSomeOne: function ([...p], firstName_parm, lastName_parm) {
    p.filter(
      e => e.first_name == firstName_parm && e.last_name == lastName_parm
    );
  },

  matchInfo: function ([...p], name_film_parm) {
    const array = [];
    p.map(function (e) {
      if (e.pref_movie.includes(name_film_parm)) {
        array.push(e);
      }
    });
    return array.length;
  },

  getDistanceFromLatLonInKm: function (lat1, lon1, lat2, lon2) {
    var R = 6371;
    var dLat = this.deg2rad(lat2 - lat1);
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) *
        Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  },

  deg2rad: function (deg) {
    return deg * (Math.PI / 180);
  },
  //===========Puedo reutilizar las funciones de arriba==========

  dramaLove: function ([...p]) {
    const loveDrama = p.filter(function (e) {
      const string = e.pref_movie;
      const regex = /(Drama)/g;
      const found = string.match(regex);
      let comparison = "";
      if (found) {
        comparison += "Drama";
      } else {
        comparison += "No Drama";
      }
      return comparison == "Drama";
    });
    return loveDrama;
  },
  //Drama lovers
  fammFicctionaLove: function ([...p]) {
    const loveFiction = this.allFemale([...p]).filter(function (e) {
      const string = e.pref_movie;
      const regex = /(Sci-Fi)/g;
      const found = string.match(regex);
      let comparison = "";
      if (found) {
        comparison += "Sci-Fi";
      } else {
        comparison += "No Sci-Fi";
      }
      return comparison == "Sci-Fi";
    });
    return loveFiction;
  },
  // Womens sci-fi lovers

  //=============================End Levl1==========================
  //=============================Start Levl2===========================

  docsAndMoney: function ([...p]) {
    const loveDoc = this.moneyFilter([...p], 1482).filter(function (e) {
      const string = e.pref_movie;
      const regex = /(Documentary)/g;
      const found = string.match(regex);
      let comparison = "";

      if (found) {
        comparison += "Documentary";
      } else {
        comparison += "No Documentary";
      }
      return comparison == "Documentary";
    });
    return loveDoc;
  },

  listPeopleNLM: function ([...p]) {
    return this.moneyFilter([...p], 4000).map(
      e =>
        `${e.first_name + " " + e.last_name} avec id ${
          e.id
        }, qui as un revenue personal de ${e.income}`
    );
  },

  richestMan: function ([...p]) {
    const allMale = this.allMale([...p]);
    let array = [];
    allMale.map(function (e) {
      let argent = parseFloat(e.income.substring(1));
      array.push({ name: e.first_name, id: e.id, money: argent });
    });
    const richestOreder = array.sort(function (a, b) {
      return b.money - a.money;
    });
    return richestOreder.map(
      e => `Homme plus riche est ${e.name} avec l'id ${e.id} qui a ${e.money}`
    )[0].blue;
  },

  moyanTotal: function ([...p]) {
    let array = [];
    p.forEach(function (e) {
      let money = parseFloat(e.income.substring(1));
      array.push(money);
    });
    const total = array.reduce(function (a, b) {
      return a + b;
    });
    return Math.round((total / array.length) * 100) / 100;
  },

  median: function ([...p]) {
    let array = [];
    let arraySuport = [];
    const pushMoney = p.forEach(function (e) {
      let money = Math.round(parseFloat(e.income.substring(1)));
      array.push(money);
    });
    const orderMoney = array.sort(function (a, b) {
      return b - a;
    });

    if (array.length % 2 == 0) {
      return orderMoney[p.length / 2];
    } else {
      array = arraySuport;
      pushMoney;
      let middleUpValue = array[Math.round((array.length - 1) / 2)];
      let middleDownValue = array[Math.round((array.length - 1) / 2) - 1];
      let averageMiddleValues = (middleDownValue + middleUpValue) / 2;
      return averageMiddleValues;
    }
  },

  northHemispherePeople: function ([...p], parm) {
    return p.filter(function (e) {
      return e.latitude > parm;
    });
  },

  sudhHemispherePeople: function ([...p], parm) {
    return p.filter(function (e) {
      return e.latitude < parm;
    });
  },

  salaireSud: function ([...p]) {
    let sudPeople = this.sudhHemispherePeople([...p], 0);
    let array = [];
    sudPeople.forEach(function (e) {
      let money = Math.round(parseFloat(e.income.substring(1)));
      array.push(money);
    });
    const total = array.reduce(function (a, b) {
      return a + b;
    });
    return Math.round((total / array.length) * 100) / 100;
  },
  //=============================End Levl2==========================
  ////=============================Start Levl3==========================


  getDistanceBetweenPeople: function (
    p,
    firstname_parm,
    lastname_parm,
    num_people
  ) 
   /*
     *Ma fonction nécessite quelques paramètres pour fonctionner:
     *  le "p",
     *  nom de la person,
     *  prenom de la person,
     *  Nombre de personnes dans l'ordre près de la personne 
     */
  
  {
    let unicPerson = [];
    let allArray = [];
    let allDistance = [];

    /**
     * Si le nom et le prénom sont égaux au paramètre que nous entrons,
     *  il fera un push sur un tableau et si non, il fera un push sur un autre tableau.
     * Cela me permettra d'avoir les personnes séparées de la personne en question.
     */

    p.map(function (e) {
      if (e.first_name == firstname_parm && e.last_name == lastname_parm) {
        unicPerson.push({
          latitude: e.latitude,
          longitude: e.longitude,
        });
      } else {
        allArray.push({
          latitude: e.latitude,
          longitude: e.longitude,
          lastname: e.last_name,
          name: e.first_name,
          id: e.id,
        });
      }
    });

    //Je calcule la distance entre la personne en question et toutes les autres personnes.
    //Pour calculer la distance, je le fais avec la formule mathématique ci-dessus

    for (let i = 0; i < allArray.length; i++) {
      let distanceConv = this.getDistanceFromLatLonInKm(
        unicPerson[0].latitude,
        unicPerson[0].longitude,
        allArray[i].latitude,
        allArray[i].longitude
      );
      //Ici je fais un push de la distance, de l'id, du prénom et du nom de la personne.
      allDistance.push({
        distance: distanceConv,
        lastname: allArray[i].lastname,
        first_name: allArray[i].name,
        id_inv: allArray[i].id,
      });
    }
    // Je fais un srot, pour ordonner la distance du plus petit au plus grand.

    const orderDistanceOfNumber = allDistance.sort(function (a, b) {
      return a.distance - b.distance;
    });

    //Ici, je choisis le nombre de personnes que je veux en commençant par le haut, c'est-à-dire par celle qui est la plus proche de moi.
    //Et c'est là que le paramètre du nombre de personnes entre en jeu.
    let allFirstDistances = orderDistanceOfNumber.slice(0, num_people);

    return allFirstDistances.map(
      (e, i) =>
        `La person ${
          i + 1
        } plus proche a ${firstname_parm} ${lastname_parm} est ${
          e.first_name
        } ${e.lastname} aver l'id ${e.id_inv} qui est a une distance de ${
          Math.round(e.distance * 100) / 100
        } km`
    );
  },

  getGoogleEmployers: function (p) {
    const employers = p.filter(function (e) {
      const string = e.email;
      const regex = /(google)/g;
      const found = string.match(regex);
      let comparison = "";

      if (found) {
        comparison += "google";
      } else {
        comparison += "No match";
      }
      return comparison == "google";
    });
    return employers.map(
      (e, i) =>
        `${i + 1}. ${e.first_name} ${e.last_name} avec l'id ${
          e.id
        } travaille pour google`
    );
  },

  getOldest: function ([...p]) {
    let allDates = [];
    p.map(function (e) {
      allDates.push(e.date_of_birth);
    });
    const orderBirth = allDates.sort()[0];
    return p
      .filter(e => e.date_of_birth == orderBirth)
      .map(e => `${e.first_name} ${e.last_name} qui a nee ${e.date_of_birth}`);
  },

  getYoungest: function ([...p]) {
    let allDates = [];
    p.map(function (e) {
      allDates.push(e.date_of_birth);
    });
    const orderBirth = allDates.sort().reverse()[0];
    return p
      .filter(e => e.date_of_birth == orderBirth)
      .map(e => `${e.first_name} ${e.last_name} qui a nee ${e.date_of_birth}`);
  },

  averageAge: function ([...p]) {
    let yearArray = [];
    let yearNow = new Date().toISOString().substring(0, 4);

    p.map(function (e) {
      let age = +yearNow - parseInt(e.date_of_birth.substring(0, 4));

      yearArray.push(age);
    });
    const summOfYears = yearArray.reduce((a, b) => a + b);
    const average = summOfYears / yearArray.length;

    return Math.round(average * 100) / 100;
  },

  //================================End Levl3===============================
  //================================Start Level4============================
  peopleValueGener: function ([...p]) {
    const array = [];
    const nameFavMov = [];
    const nameGen = [];
    const nameAndValues = [];

    //En gros, ici, j'enlève le signe "|", pour avoir tous les genres de films sous forme de string.
    //Puis je fais un push de tous les éléments que je récupère.
    p.map(function (e) {
      e.pref_movie.split("|").map(el => array.push(el));
    });
    //Ici, j'implémente la fonction "Set" qui retourne une seule valeur, parmi toutes celles qui ont été répétées.
    //Ej: array = ["Drama", "Adventure", "Drama", "Action", "Action"]
    //Result => array = ["Drama", "Adventure", "Action"]
    let unic = new Set(array);
    let arrGener = [...unic];


    //Je fais une poussée de tous les genres de chaque personne.
    p.map(function (ele) {
      nameFavMov.push({ name: ele.pref_movie });
    });
    //Si les genres de films de la personne incluent l'un de ces genres, je la pousserai vers un autre tableau.
    arrGener.map(function (ele) {
      nameFavMov.map(function (e) {
        if (e.name.includes(ele) === true) {
          nameGen.push({ name: ele });
        }
      });
    });
    //Pour chaque sexe que j'ai, je récupère le numéro de chaque personne qui comprend ce sexe et je fais une dernière poussée.
    for (let i = 0; i < arrGener.length; i++) {
      let numberPeople = nameGen.filter(e => e.name == arrGener[i]).length;

      nameAndValues.push({ name: arrGener[i], value: numberPeople });
    }

    //Enfin, je classe les valeurs du plus grand au plus petit.

    const order = nameAndValues.sort((a, b) => b.value - a.value);
    return order;

    //Je peux ensuite réutiliser cette fonction pour de futurs exercices.
  },

  getPopularFilm: function ([...p]) {
    const order = this.peopleValueGener([...p]);
    return order.map(
      e =>
        `Genre de film le plus populaire este ${e.name} qui est aimé par ${e.value} persons`
    )[0].blue;
  },

  orderPopularGener: function ([...p]) {
    const order = this.peopleValueGener([...p]);
    return order.map(
      (e, i) => `${i + 1}. Genre de film le plus populaire este ${e.name}`
    );
  },

  orderPopularGenerAndPeoplesNumber: function ([...p]) {
    const order = this.peopleValueGener([...p]);
    return order.map(
      (e, i) =>
        `${i + 1}. Genre de film le plus populaire este ${
          e.name
        } qui est aimé par ${e.value} persons`
    );
  },

  averageAgeFilm: function ([...p], film_param, genre) {
    let yearArray = [];
    let yearNow = new Date().toISOString().substring(0, 4);

    this.filterFilm([...p], film_param, genre).map(function (e) {
      let age = +yearNow - parseInt(e.date_of_birth.substring(0, 4));
      yearArray.push(age);
    });
    const summOfYears = yearArray.reduce((a, b) => a + b);
    const average = summOfYears / yearArray.length;

    return Math.round(average * 100) / 100;
  },

  averageAgeFilmFemParis: function ([...p], film_param, genre, lat1, lon1) {
    /*
     *Ma fonction nécessite quelques paramètres pour fonctionner:
     *  le "p",
     *  le nom du film,
     *  le sexe (masculin ou féminin),
     *  la latitude du site en question
     * et la longitude.
     */

    let array = [];
    let allArrayFiltered = [];
    let allArrayTimeZone = [];
    let peopleInSameTimeZone = [];
    let arrayMenMoeny = [];

    /*
     *Ici, je fais essentiellement un map, pour filtre de toutes les "femmes" (dans ce cas, parce que c'est un paramètre),
     *  et je regarde si elles contiennent le film "Film-Noir" (qui est aussi un paramètre).
     */

    genre.map(function (e) {
      if (e.pref_movie.includes(film_param)) {
        array.push(e);
      }
    });

    /*
     *Ici, je fais une map, je transforme l'argent en un nombre et je pousse ce qui m'intéresse comme un objet vers un autre tableau.
     */

    array.map(function (e) {
      let money = parseFloat(e.income.substring(1));

      allArrayFiltered.push({
        latitude: e.latitude,
        longitude: e.longitude,
        lastname: e.last_name,
        name: e.first_name,
        id: e.id,
        birthday: e.date_of_birth,
        films: e.pref_movie,
        money: money,
      });
    });
    /*
     * Maintenant une boucle for pour chaque élément du allArrayFiltered
     */
    for (let i = 0; i < allArrayFiltered.length; i++) {
      // J'applique le module installé, ci-dessus je vous montre ce que la méthode "find" retourne, je l'applique pour chacun des éléments.
      // Ensuite j'utilise la fonction "new Date()" de l'heure locale et cela me permet de connaître l'heure et les données du lieu que je mets en question;

      //Ej:

      // const zoneTime = find(47.650499, -122.35007);  ==> ['America/Los_Angeles']
      //const hourAndDateZone = new Date().toLocaleString('fr-FR', { timeZone: zoneTime });
      //où "zoneTime" est égal à la string, dans le array qui me retourne
      //hourAndDateZone, retournera quelque chose comme "03/12/2021, 13:54:54"

      const zonetime = find(
        allArrayFiltered[i].latitude,
        allArrayFiltered[i].longitude
      );
      const hourAndDateZone = new Date().toLocaleString("fr-FR", {
        timeZone: zonetime,
      });
      allArrayTimeZone.push({
        timezone: zonetime,
        date_hour_zone: hourAndDateZone,
        lastname: allArrayFiltered[i].lastname,
        name: allArrayFiltered[i].name,
        id: allArrayFiltered[i].id,
        birthday: allArrayFiltered[i].birthday,
        films: allArrayFiltered[i].films,
        money: allArrayFiltered[i].money,
      });
    }

    //Je crée à nouveau un objet avec les informations qui m'intéressent, mais plus "zoneTime", et "hourAndDateZone".

    const zone = find(lat1, lon1);
    const dateZone = new Date().toLocaleString("fr-FR", { timeZone: zone });

    //Je fais de même pour récupérer l'heure et la date du lieu que j'ai demandé (dans ce cas, il s'agit de Paris).
    //Qui me rend l'heure et la date de Paris, par exemple ==> 03/12/2021, 23:00:42
    //Je peux ainsi comparer les dates et les heures et, si elles sont identiques, cela signifie qu'elles se trouvent dans le même fuseau horaire.
    // Si je me souviens bien, une seule personne se trouve dans le même fuseau horaire que Paris

    allArrayTimeZone.map(function (e) {
      if (e.date_hour_zone === dateZone) {
        // Dans le tableau "peopleInSameTimeZone", je place toutes les personnes que j'ai récupérées lors de la comparaison des fuseaux horaires.
        peopleInSameTimeZone.push(e);
      }
    });

    //Ici, je fais la médiane de l'argent de tous les hommes.

    this.allMale([...p]).forEach(function (e) {
      let money = parseFloat(e.income.substring(1));
      arrayMenMoeny.push(money);
    });
    const total = arrayMenMoeny.reduce(function (a, b) {
      return a + b;
    });
    const allMenMoney = Math.round((total / arrayMenMoeny.length) * 100) / 100;

    //Ici, je filtre essentiellement les femmes qui, dans ce cas, sont moins payées que l'homme moyen.

    const moneyFilter = peopleInSameTimeZone.filter(e => e.money < allMenMoney);

    //Ici, je prends les âges de toutes les personnes retournées par le filtre précédent et j'additionne leurs âges pour les diviser par 2.
    //Et obtenez la moyenne pour les femmes
    //Il est vrai qu'elle ne renvoie qu'une seule femme, mais même ainsi,
    //si vous changez ensuite les paramètres pour réutiliser la fonction, la moyenne sera atteinte.

    let yearArray = [];
    let yearNow = new Date().toISOString().substring(0, 4);
    moneyFilter.map(function (e) {
      let age = +yearNow - parseInt(e.birthday.substring(0, 4));
      yearArray.push(age);
    });
    const summOfYears = yearArray.reduce((a, b) => a + b);
    const average = summOfYears / yearArray.length;
    return Math.round(average * 100) / 100;
  },

  ManAndMan: function ([...p]) {
    console.log(
      "Ne vous inquiétez pas, il ne prend qu'un certain temps à s'exécuter en raison des calculs qu'il effectue, il n'y a pas de boucle qui va à l'infini."
        .yellow
    );

    const manInterest = this.nbOfMaleInterest([...p]);
    const manLookFormMan = manInterest.filter(e => e.gender == "Male");
    const allDistance = [];
    const allDistanceBetween = [];
    
    //Pour chaque homme cherchant un homme, je crée une boucle qui exécutera une autre boucle.
    //Donc, pour chaque homme qui cherche un homme, je vais chercher toutes les distances avec d'autres hommes.
    for (let j = 0; j < manLookFormMan.length; j++) {
      for (let i = 0; i < manLookFormMan.length; i++) {
        let distanceConv = this.getDistanceFromLatLonInKm(
          manLookFormMan[j].latitude,
          manLookFormMan[j].longitude,
          manLookFormMan[i].latitude,
          manLookFormMan[i].longitude
        );
        //Ici je fais une push, de tous les résultats précédents, disculpant la même personne.
        if (manLookFormMan[j].first_name != manLookFormMan[i].first_name) {
          allDistance.push({
            distance: distanceConv,
            firstname: manLookFormMan[j].first_name,
            lastname: manLookFormMan[j].last_name,
            id: manLookFormMan[j].id,
            gender_movie: manLookFormMan[j].pref_movie,
            second_firstname: manLookFormMan[i].first_name,
            second_lastname: manLookFormMan[i].last_name,
            second_id: manLookFormMan[i].id,
            second_gender_movie: manLookFormMan[i].pref_movie,
           
          });
        }
      }
    }

    //Si vous faites un "console.log" de "allDistance", vous verrez qu'il retourne tout mélangé.
    //Donc pour faire un peu de ménage, j'applique la méthode "find", qui n'appartient pas à la bibliothèque que j'ai installée.
    //Ensuite, j'applique la méthode some qui renvoie un "booléen"
    //Pour ensuite faire une condition grâce au booléen récupéré ci-dessus
    //avec cela je récupère tout séparément en respectant les conditions énoncées dans la méthode "some"

    
    const splitted = allDistance.reduce((a, b) => {
      const match = a.find(e =>
        e.some(el => el.firstname === b.firstname && el.lastname === b.lastname && b.gender_movie === b.second_gender_movie)
      );
      if (match) {
        match.push(b);
      } else {
        a.push([b]);
      }
      return a;
    }, []);

    //Si ce n'est pas clair ci-dessus, faites un "console.log" de allDistance et splitted, pour voir la différence.
    
    //Ci-dessous je fais une boucle "for" récupérant la distance la plus proche, puisque j'utilise la méthode "sort" pour ordener
    //du plus petit au plus grand et l'index 0 pour récupérer la plus petite distance.
    //Comme j'ai précédemment évité l'information de la même personne, puisque sa distance à elle-même est 0,
    //je sais à 100% que l'indice 0 est la personne la plus proche. 
    for (let i = 0; i < splitted.length; i++) {
      const order = splitted[i].sort((a, b) => a.distance - b.distance)[0];
      if(order.second_gender_movie == order.gender_movie){
      allDistanceBetween.push({
        distance: order.distance,
        firstname: order.firstname,
        lastname: order.lastname,
        id: order.id,
        gender_movie: order.gender_movie,
        second_firstname: order.second_firstname,
        second_lastname: order.second_lastname,
        second_id: order.second_id,
        second_gender_movie : order.second_gender_movie,
      });
    }
    }  
    
    //Ici, je crée la phrase
    //Je reçois des couples d'hommes à la recherche d'hommes, qui ont en commun les mêmes genres de films.
    //Et il me renvoie au plus près du plus près possible.

    return allDistanceBetween.map(
      e =>
        `Un partenaire potentiel est ${e.firstname} ${e.lastname} qui aime ${e.gender_movie} et ${
          e.second_firstname
        } ${e.second_lastname} qui aime ${e.second_gender_movie}, sont à une distance de ${
          Math.round(e.distance * 100) / 100
        } km`
    );

  },

  manAndWoman: function ([...p]) {
    //Cet exercice fonctionne de la même manière que le précédent
    const heteros = this.allFemale([...p])
      .filter(e => e.looking_for == "M")
      .concat(this.allMale([...p]).filter(e => e.looking_for == "F"));
    const allGender = this.peopleValueGener([...p]).map(e => e.name);
    const test = [];

    for (let j = 0; j < allGender.length; j++) {
      for (let i = 0; i < heteros.length; i++) {
        if (heteros[i].pref_movie.includes(allGender[j]) == true) {
          let nameGen = allGender[j];
          test.push({
            firstname: heteros[i].first_name,
            lastname: heteros[i].last_name,
            id: heteros[i].id,
            pref_movie: heteros[i].pref_movie,
            move_comun: nameGen,
            gender: heteros[i].gender,
            looking_for: heteros[i].looking_for,
          });
        }
      }
    }
    let packCouple = test.reduce(function (a, b) {
      let key = `Couples possibles qui ont des films ${b.move_comun} en commun`;
      if (a[key] == null) a[key] = [];

      a[key].push(b);
      return a;
    }, {});
    
    //Pour voir la réponse, décommenter la ligne ci-dessous
    // return packCouple

    return "Pour voir le résultat de cet exercice, vous devez décommenter le 'return' du résultat, car la réponse est très longue.".yellow
  },

  //================================End Lever4==============================
    //Celui-là, je le ferai de mon côté.
    match: function([...p]){
        return "not implemented".red;
    }

};
